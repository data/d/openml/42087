# OpenML dataset: beer_reviews

https://www.openml.org/d/42087

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset consists of beer reviews from Beeradvocate. The data span a period of more than 10 years, including all ~1.5 million reviews up to November 2011. Each review includes ratings in terms of five "aspects": appearance, aroma, palate, taste, and overall impression. Reviews include product and user information, followed by each of these five ratings, and a plaintext review. We also have reviews from ratebeer.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42087) of an [OpenML dataset](https://www.openml.org/d/42087). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42087/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42087/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42087/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

